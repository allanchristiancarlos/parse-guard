<?php

namespace Korvipe\ParseGuard;

use Parse\ParseObject;
use Parse\ParseUser;
use Parse\ParseRole;

class ParseGuard
{
    /**
     * Check if the user has role
     * @param  string|ParseRole    $roleName The role to check
     * @param  ParseUser           $user     The user to check
     * @return boolean             
     */
    public function hasRole($roleName, ParseUser $user)
    {
        $role = null;

        if ($roleName instanceof ParseRole) {
            $role = $roleName;
        } else {
            try {
                $role = ParseRole::query()
                        ->equalTo('name', $roleName)
                        ->limit(1)
                        ->first();
            } catch (\Exception $e) {
                return false;
            }
        }
        
        if (empty($role)) {
            return false;
        }

        try {
            $count = $role->getUsers()->getQuery()
                        ->equalTo('objectId', $user->getObjectId())
                        ->limit(1)
                        ->count();
            return (bool) $count;
        } catch (\Exception $e) {
            return false;
        }
    }
}


