<?php

namespace Korvipe\ParseGuard;

use Illuminate\Support\ServiceProvider;
use Validator;

class LaravelServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Validator::extend('parse_unique', 
            'Korvipe\ParseGuard\Validators\UniqueValidator@validate',
            ':attribute already exists');

        Validator::extend('parse_exists', 
            'Korvipe\ParseGuard\Validators\ExistsValidator@validate',
            ':attribute does not exists.');
    }

    public function register()
    {
        $this->app->bind('parseguard', function($app) {
            return new ParseGuard;
        });

        $this->app->alias('parseguard', 'Korvipe\ParseGuard\ParseGuard');
    }
}
