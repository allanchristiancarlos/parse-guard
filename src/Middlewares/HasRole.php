<?php

namespace Korvipe\ParseGuard\Middlewares;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Korvipe\ParseGuard\Facades\ParseGuard;
use Parse\ParseUser;
use Closure;

class HasRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if ($user = ParseUser::getCurrentUser()) {
            if (!ParseGuard::hasRole($role, $user)) {
                throw new NotFoundHttpException;
            }
        } else {
            throw new NotFoundHttpException;
        }

        return $next($request);
    }
}