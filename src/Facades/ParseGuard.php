<?php

namespace Korvipe\ParseGuard\Facades;

use Illuminate\Support\Facades\Facade;

class ParseGuard extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'parseguard';
    }
}