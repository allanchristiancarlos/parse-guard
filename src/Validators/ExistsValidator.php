<?php

namespace Korvipe\ParseGuard\Validators;

use Parse\ParseQuery;

class ExistsValidator
{
    public function validate($attribute, $value, $parameters, $validator)
    {
        $class  = $parameters[0];
        $column = isset($parameters[1]) ? $parameters[1] : 'objectId';

        $query = new ParseQuery($class);
        $query->equalTo($column, $value);
        $query->limit(1);

        return (bool) $query->count();
    }
}