<?php

namespace Korvipe\ParseGuard\Validators;

use Parse\ParseQuery;

class UniqueValidator
{
    public function validate($attribute, $value, $parameters, $validator)
    {
        $class  = $parameters[0];
        $column = isset($parameters[1]) ? $parameters[1] : 'objectId';

        $query = new ParseQuery($class);
        $query->equalTo($column, $value);

        // Except id
        if (isset($parameters[2])) {
            $except   = $parameters[2];
            $idColumn = isset($parameters[3]) ? $parameters[3] : 'objectId';

            $query->notEqualTo($idColumn, $except);
        }

        $query->limit(1);

        return ! ((bool) $query->count());
    }
}
